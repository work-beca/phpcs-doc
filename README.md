# DOCUMENTATION PHPCS #

Esta documentación sirve para tener una pequeña guia a la hora de comentar algun error phpcs que no se pueda solventar mopdificando código o se prefiera dejar sin tocar.

El formato para escribir un comentario es el siguiente:

`Code Line {//phpcs:ignore}{espacio}{comentario para ignorar}`

Por ejemplo imaginemos que queremos comentar el acceso directo a la bbdd. Para ello utilizariamos el siguiente comentario después de la linea de codigo que se quiere comentar:

`//phpcs:ignore WordPress.DB.DirectDatabaseQuery`

*En el caso de que no sepamos que comentario utilizar siempre podemos poner phpcs:ignore como norma general. Esto hará que desaparezca cualquier error phpcs.*

También podemos indicar si queremos ignorar un bloque de código o un conjunto de líneas usando lo siguiente:

```
// @codingStandardsIgnoreStart

  Codigo que queremos ignorar

// @codingStandardsIgnoreEnd
```

### **A partir de aqui se añadirán los diferentes comentarios que he ido utilizando asi como todos aquellos que he encontrado y que puede ser interesante su uso.**
&nbsp;

Los comentarios pueden ser más específicos segun el error phpcs que se nos indica. A veces bajar a un nivel de especificidad muy grande puede hacer que no nos funcione por lo que a veces es mejor usar un nivel superior o subir niveles hasta que el error desaparezca.

### WordPress.NamingConventions ###

* WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase


### WordPress.Security ###

* WordPress.Security.NonceVerification
* WordPress.Security.EscapeOutput
* WordPress.Security.ValidatedSanitizedInput.InputNotSanitized


### WordPress.PHP ###

* WordPress.PHP.NoSilencedErrors.Discouraged
* WordPress.PHP.DontExtract
* Other guidelines

### WordPress.DB ###

* WordPress.DB.DirectDatabaseQuery
* WordPress.DB.PreparedSQL.NotPrepared
* WordPress.DB.PreparedSQL.InterpolatedNotPrepared
* slow query ok.

### WordPress.WP ###

* WordPress.WP.GlobalVariablesOverride